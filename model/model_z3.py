from z3 import *

#def BitVecVal(n, w): return BitVecVal(n, w)
def BVType(w): return BitVecSort(w)
def ArrayType(k,v): return ArraySort(k,v)
def Symbol(n,t): return Const(n, t)
def BOOL(): return BoolSort()
def Equals(a,b): return a == b
def BVUGT(a,b): return UGT(a,b)
def BVULT(a,b): return ULT(a,b)
def BVAdd(a,b): return a + b
def BVUGE(a,b): return UGE(a,b)
def BVSub(a,b): return a-b
def BVZExt(a, ext): return Concat(BitVecVal(0, ext), a)
def BVUDiv(a, b): return UDiv(a, b)
def BVMul(a, b): return a * b
def BVURem(a, b): return URem(a,b)
def BVConcat(a, b): return Concat(a, b)
def Ite(i, t, e): return If(i, t, e)
def BVULE(a, b): return ULE(a, b)
def TRUE(): return True

NumOfSteps       = 2       # maximum number of steps in a trace
BitWidth         = 24      # maximum uint value 2^BitWidth
UintWidth        = BitWidth
TimeWidth        = 16
AddrWidth        = 3
NumOfAddresses   = 4     # number of participants (w/o miniDAO)

# a proposal deadline duration
deadlineDuration = BitVecVal(10000, TimeWidth)
consensus        = BitVecVal(51,  BitWidth)
daoTokenPrice    = BitVecVal(2,   BitWidth)
Zero = BitVecVal(0, BitWidth)
One  = BitVecVal(1, BitWidth)
noAddr        = BitVecVal(0,AddrWidth)
miniDAO_contract_address = BitVecVal(NumOfAddresses + 1, AddrWidth)
BoolTrue = BitVecVal(1, 1)
BoolFalse = BitVecVal(0, 1)

AddressType   = BVType(AddrWidth)
BoolType = BVType(1)
Uint = BVType(BitWidth)
Uint5  = BVType(5)
Uint8  = BVType(8)
Uint16 = BVType(16)
Uint32 = BVType(32)
Time = BVType(TimeWidth)

MappingAddressUint = ArrayType(AddressType, Uint)
ProposalAddressBV = BVType(16+AddrWidth)
MappingPABVBool = ArrayType(ProposalAddressBV, BoolType)

## 2^4 = 16 different events can be defined
ETWidth = 4
EventNameType    = BVType(ETWidth)
NoEvent = BitVecVal(0, ETWidth)
ProposalAdded = BitVecVal(1, ETWidth)
ProposalExecuted = BitVecVal(2, ETWidth)
ProposalNoConsensusYet = BitVecVal(3, ETWidth)
ProposalRejected = BitVecVal(4, ETWidth)
Voted = BitVecVal(5, ETWidth)
Refunded = BitVecVal(6, ETWidth)
Deposited = BitVecVal(7, ETWidth)
ERC20_Transfer = BitVecVal(8, ETWidth)

##=====================================================
## Contract state variables
##=====================================================
proposal_recepient = \
 [Symbol('proposal_recepient_' + str(i), AddressType)
  for i in range(NumOfSteps)]
proposal_amount = \
 [Symbol('proposal_amount_' + str(i), Uint)
  for i in range(NumOfSteps)]
proposal_deadline = \
 [Symbol('proposal_deadline_' + str(i), Time)
  for i in range(NumOfSteps)]
daoTokensEmitted = \
 [Symbol('daoTokensEmitted_' + str(i), Uint)
  for i in range(NumOfSteps)]
proposalID = \
 [Symbol('proposalID_' + str(i), Uint16)
  for i in range(NumOfSteps)]            
votedYes = \
 [Symbol('votedYes_' + str(i), Uint)
  for i in range(NumOfSteps)]                       
votedNo = \
 [Symbol('votedNo_' + str(i), Uint)
  for i in range(NumOfSteps)]                       
daoBalance = \
 [Symbol('daoBalance_' + str(i), MappingAddressUint)
  for i in range(NumOfSteps)]
isVoted = \
 [Symbol('isVoted_' + str(i), MappingPABVBool)
  for i in range(NumOfSteps)]
logs_event = \
 [Symbol('logs_event_' + str(i), EventNameType)
  for i in range(NumOfSteps)]             
logs_arg1 = \
 [Symbol('logs_arg1_' + str(i), AddressType)
  for i in range(NumOfSteps)]            
logs_arg2 = \
 [Symbol('logs_arg2_' + str(i), Uint)
  for i in range(NumOfSteps)]            
logs_arg3 = \
 [Symbol('logs_arg3_' + str(i), Uint)
  for i in range(NumOfSteps)]            

##=====================================================
##Call arguments
##=====================================================
propose_recepient_arg = \
 [Symbol('propose_recepient_arg_' + str(i), AddressType)
  for i in range(NumOfSteps)]
propose_amount_arg = \
 [Symbol('propose_amount_arg_' + str(i), Uint)
  for i in range(NumOfSteps)]
vote_proposalID_arg = \
 [Symbol('vote_proposalID_' + str(i), Uint16)
  for i in range(NumOfSteps)]
vote_supportsProposal_arg = \
 [Symbol('vote_supportsProposal_' + str(i), BoolSort())
  for i in range(NumOfSteps)]
erc20_transfer_tokens_arg = \
 [Symbol('erc20_transfer_tokens_' + str(i), Uint)
  for i in range(NumOfSteps)]
erc20_transfer_to_arg = \
 [Symbol('erc20_transfer_to_' + str(i), AddressType)
  for i in range(NumOfSteps)]


time_delta = \
 [Symbol('time_delta_' + str(i), Time)
  for i in range(NumOfSteps)]             
msg_value = \
 [Symbol('msg_value_' + str(i), Uint)
  for i in range(NumOfSteps)]
msg_sender = \
 [Symbol('msg_sender_' + str(i), AddressType)
  for i in range(NumOfSteps)]


##=====================================================
##System state
##=====================================================
ether_balance = \
 [Symbol('ether_balance_' + str(i), MappingAddressUint)
  for i in range(NumOfSteps)]
now = \
 [Symbol('now_' + str(i), Time)
  for i in range(NumOfSteps)]

##=====================================================
##Initial state
##=====================================================

daoTokensEmitted_initial = Zero
proposalID_initial = BitVecVal(1, 16) # 0 is reserved
votedYes_initial = Zero
votedNo_initial = Zero
daoBalance_initial = K(AddressType, Zero)
isVoted_initial = K(ProposalAddressBV, BoolFalse)
proposal_recepient_initial = noAddr
proposal_amount_initial = Zero
proposal_deadline_initial = BitVecVal(0, TimeWidth)
logs_event_initial = NoEvent
logs_arg1_initial = noAddr
logs_arg2_initial = Zero
logs_arg3_initial = Zero
ether_balance_initial = Symbol('ether_balance_initial',
                               MappingAddressUint)
now_initial = Symbol('now_initial', Time)

## State of the system:
# (daoTokensEmitted,
#  proposalID,
#  votedYes,
#  votedNo,
#  daoBalance,
#  isVoted,
#  proposal_recepient,
#  proposal_amount,
#  proposal_deadline,
#  logs_event,
#  logs_arg1,
#  logs_arg2,
#  logs_arg3,
#  ether_balance,
#  now)

def propose_precond(i):
    return And(Equals(proposal_recepient[i], noAddr),
               Not(Equals(propose_recepient_arg[i], noAddr)),
               Not(Equals(propose_amount_arg[i], Zero)))

def BVZExt1(v, w):
    if w > 0: return BVZExt(v,w)
    else: return v
    
def propose(i):
    return And(
        Equals(daoTokensEmitted[i+1], daoTokensEmitted[i]),
        Equals(proposalID[i+1], proposalID[i]),
        Equals(votedYes[i+1], votedYes[i]),
        Equals(votedNo[i+1], votedNo[i]),
        Equals(daoBalance[i+1], daoBalance[i]),
        Equals(isVoted[i+1], isVoted[i]),
        Equals(proposal_recepient[i+1],
               propose_recepient_arg[i]),
        Equals(proposal_amount[i+1], propose_amount_arg[i]),
        Equals(proposal_deadline[i+1],
               BVAdd(now[i], deadlineDuration)),
        Equals(logs_event[i+1], ProposalAdded),
        Equals(logs_arg1[i+1], msg_sender[i]),
        Equals(logs_arg2[i+1], propose_amount_arg[i]),
        Equals(logs_arg3[i+1], BVZExt1(proposalID[i], UintWidth - 16)),
        Equals(ether_balance[i+1], ether_balance[i]),
        Equals(now[i+1], time_delta[i]))

def deposit_precond(i):
    tokens = BVUDiv(msg_value[i], daoTokenPrice)
    daoBalanceSender = Select(daoBalance[i], msg_sender[i])
    ethBalanceSender = Select(ether_balance[i], msg_sender[i])
    ethBalanceContract = Select(ether_balance[i],
                                miniDAO_contract_address)
    return And(BVUGE(msg_value[i], daoTokenPrice),
               BVUGE(ethBalanceSender, msg_value[i]),
               BVUGT(BVAdd(ethBalanceSender, msg_value[i]),
                   ethBalanceSender),
               BVUGT(BVAdd(ethBalanceContract,
                          BVMul(tokens,daoTokenPrice)),
                   ethBalanceContract),
               BVUGT(BVAdd(daoBalanceSender, tokens),
                     daoBalanceSender),
               BVUGT(BVAdd(daoTokensEmitted[i], tokens),
                     daoTokensEmitted[i]))

def deposit(i):
    tokens = BVUDiv(msg_value[i], daoTokenPrice)
    remainder = BVURem(msg_value[i], daoTokenPrice)
    daoBalanceSender1 = \
        BVAdd(Select(daoBalance[i], msg_sender[i]), tokens)
    daoBalance1 = Store(daoBalance[i], msg_sender[i],
                       daoBalanceSender1)
    daoTokensEmitted1 = BVAdd(daoTokensEmitted[i], tokens)
    newSenderBalanceVal = \
        BVSub(BVAdd(Select(ether_balance[i], msg_sender[i]),
                    remainder),
              msg_value[i])
    newContractBalanceVal = \
        BVSub(BVAdd(Select(ether_balance[i], miniDAO_contract_address),
                    msg_value[i]),
              remainder)
    etherBalance1 = Store(ether_balance[i], msg_sender[i],
                          newSenderBalanceVal)
    etherBalance2 = Store(etherBalance1,
                          miniDAO_contract_address,
                          newContractBalanceVal)
    return And(
        Equals(daoTokensEmitted[i+1], daoTokensEmitted1),
        Equals(proposalID[i+1], proposalID[i]),
        Equals(votedYes[i+1], votedYes[i]),
        Equals(votedNo[i+1], votedNo[i]),
        Equals(proposal_recepient[i+1], proposal_recepient[i]),
        Equals(proposal_amount[i+1], proposal_amount[i]),
        Equals(proposal_deadline[i+1], proposal_deadline[i]),
        Equals(daoBalance[i+1], daoBalance1),
        Equals(isVoted[i+1], isVoted[i]),
        Equals(logs_event[i+1], Deposited),
        Equals(logs_arg1[i+1], msg_sender[i]),
        Equals(logs_arg2[i+1], tokens),
        Equals(logs_arg3[i+1], Zero),
        Equals(ether_balance[i+1], etherBalance2),
        Equals(now[i+1], time_delta[i])
    )

def vote_precond(i):
    tokens = Select(daoBalance[i], msg_sender[i])
    Key = BVConcat(vote_proposalID_arg[i], msg_sender[i])
    isVotedB = Select(isVoted[i], Key)
    return And(Equals(vote_proposalID_arg[i], proposalID[i]),
               Not(Equals(proposal_recepient[i], noAddr)),
               BVULT(now[i], proposal_deadline[i]),
               Equals(isVotedB, BoolFalse),
               BVUGT(BVAdd(votedYes[i], tokens), votedYes[i]),
               BVUGT(BVAdd(votedNo[i], tokens), votedNo[i]))

def vote(i):
    tokens = Select(daoBalance[i], msg_sender[i])
    Key = BVConcat(vote_proposalID_arg[i], msg_sender[i])
    Key0 = BVConcat(BitVecVal(0, 16),  msg_sender[i])
    isVoted1 = Store(isVoted[i], Key, BoolTrue)
    isVoted2 = Store(isVoted1, Key0, BoolTrue)
    #daoBalance1 = Store(daoBalance[i], msg_sender[i], Zero)

    supports_true = And(
        Equals(daoTokensEmitted[i+1], daoTokensEmitted[i]),
        Equals(proposalID[i+1], proposalID[i]),
        Equals(votedYes[i+1], BVAdd(votedYes[i], tokens)),
        Equals(votedNo[i+1], votedNo[i]),
        Equals(proposal_recepient[i+1], proposal_recepient[i]),
        Equals(proposal_amount[i+1], proposal_amount[i]),
        Equals(proposal_deadline[i+1], proposal_deadline[i]),
        Equals(daoBalance[i+1], daoBalance[i]),
        Equals(isVoted[i+1], isVoted2),
        Equals(logs_event[i+1], Voted),
        Equals(logs_arg1[i+1], msg_sender[i]),
        Equals(logs_arg2[i+1], BVZExt1(vote_proposalID_arg[i], UintWidth - 16)),
        Equals(logs_arg3[i+1], One),
        Equals(ether_balance[i+1], ether_balance[i]),
        Equals(now[i+1], time_delta[i])
    )

    supports_false = And(
        Equals(daoTokensEmitted[i+1], daoTokensEmitted[i]),
        Equals(proposalID[i+1], proposalID[i]),
        Equals(votedYes[i+1], votedYes[i]),
        Equals(votedNo[i+1], BVAdd(votedNo[i], tokens)),
        Equals(proposal_recepient[i+1], proposal_recepient[i]),
        Equals(proposal_amount[i+1], proposal_amount[i]),
        Equals(proposal_deadline[i+1], proposal_deadline[i]),
        Equals(daoBalance[i+1], daoBalance[i]),
        Equals(isVoted[i+1], isVoted2),
        Equals(logs_event[i+1], Voted),
        Equals(logs_arg1[i+1], msg_sender[i]),
        Equals(logs_arg2[i+1], BVZExt1(vote_proposalID_arg[i], UintWidth - 16)),
        Equals(logs_arg3[i+1], Zero),
        Equals(ether_balance[i+1], ether_balance[i]),
        Equals(now[i+1], time_delta[i])
    )
    return Ite(vote_supportsProposal_arg[i],
               supports_true,
               supports_false)

def refund_precond(i):
    Key = BVConcat(BitVecVal(0, 16), msg_sender[i]) 
    daoBalance1 = Select(daoBalance[i], msg_sender[i])
    contractBalance1 = Select(ether_balance[i],
                             miniDAO_contract_address)
    isVotedB = Select(isVoted[i], Key)
    return And(
        Equals(isVotedB, BoolFalse),
        BVUGT(daoBalance1, Zero),
        BVUGE(daoTokensEmitted[i], daoBalance1),
        BVUGE(contractBalance1, BVMul(daoBalance1,
                                      daoTokenPrice))
    )

def refund(i):
    tokens = Select(daoBalance[i], msg_sender[i])
    contractBalance = Select(ether_balance[i],
                             miniDAO_contract_address)
    senderBalance = Select(ether_balance[i],
                           msg_sender[i])
    ethBalance1 = Store(ether_balance[i],
                        miniDAO_contract_address,
                        BVSub(contractBalance,
                              BVMul(tokens, daoTokenPrice)))
    ethBalance2 = Store(ethBalance1,
                        msg_sender[i],
                        BVAdd(senderBalance,
                              BVMul(tokens, daoTokenPrice)))
    daoTokensEmitted1 = BVSub(daoTokensEmitted[i], tokens)
    daoBalance1 = Store(daoBalance[i], msg_sender[i], Zero)
    return And(
        Equals(daoTokensEmitted[i+1], daoTokensEmitted1),
        Equals(proposalID[i+1], proposalID[i]),
        Equals(votedYes[i+1], votedYes[i]),
        Equals(votedNo[i+1], votedNo[i]),
        Equals(proposal_recepient[i+1], proposal_recepient[i]),
        Equals(proposal_amount[i+1], proposal_amount[i]),
        Equals(proposal_deadline[i+1], proposal_deadline[i]),
        Equals(daoBalance[i+1], daoBalance1),
        Equals(isVoted[i+1], isVoted[i]),
        Equals(logs_event[i+1], Refunded),
        Equals(logs_arg1[i+1], msg_sender[i]),
        Equals(logs_arg2[i+1], tokens),
        Equals(logs_arg3[i+1], Zero),
        Equals(ether_balance[i+1], ethBalance2),
        Equals(now[i+1], time_delta[i])
    )

def execute_proposal_precond(i):
    return And(Not(Equals(proposal_recepient[i],noAddr)),
               BVUGT(daoTokensEmitted[i], Zero))

def execute_proposal(i):
    contractBalance = Select(ether_balance[i],
                             miniDAO_contract_address)
    recepientBalance = Select(ether_balance[i],
                              proposal_recepient[i])
    isPositiveDecision = BVUGT(votedYes[i], votedNo[i])
    isConsensusReached = \
        BVUGE(BVUDiv(BVMul(BitVecVal(100, BitWidth),
                           BVAdd(votedNo[i], votedYes[i])),
                     daoTokensEmitted[i]),
              consensus)
    isSufficientFunds = BVULE(proposal_amount[i], contractBalance)
    ethBalance1 = Store(ether_balance[i], proposal_recepient[i],
                        BVAdd(recepientBalance, proposal_amount[i]))
    ethBalance2 = Store(ethBalance1,
                        miniDAO_contract_address,
                        BVSub(contractBalance,
                              proposal_amount[i]))
    executed = And(
        Equals(daoTokensEmitted[i+1], daoTokensEmitted[i]),
        Equals(proposalID[i+1], BVAdd(proposalID[i], BitVecVal(1,16))),
        Equals(votedYes[i+1], votedYes_initial),
        Equals(votedNo[i+1], votedNo_initial),
        Equals(proposal_recepient[i+1], noAddr),
        Equals(proposal_amount[i+1], Zero),
        Equals(proposal_deadline[i+1], BitVecVal(0, TimeWidth)),
        Equals(daoBalance[i+1], daoBalance[i]),
        Equals(isVoted[i+1], isVoted[i]),
        Equals(logs_event[i+1], ProposalExecuted),
        Equals(logs_arg1[i+1], noAddr),
        Equals(logs_arg2[i+1], BVZExt1(proposalID[i],UintWidth - 16)),
        Equals(logs_arg3[i+1], Zero),
        Equals(ether_balance[i+1], ethBalance2),
        Equals(now[i+1], time_delta[i])
    )

    rejected = And(
        Equals(daoTokensEmitted[i+1], daoTokensEmitted[i]),
        Equals(proposalID[i+1], proposalID[i]),
        Equals(votedYes[i+1], votedYes_initial),
        Equals(votedNo[i+1], votedNo_initial),
        Equals(proposal_recepient[i+1], noAddr),
        Equals(proposal_amount[i+1], Zero),
        Equals(proposal_deadline[i+1], BitVecVal(0,TimeWidth)),
        Equals(daoBalance[i+1], daoBalance[i]),
        Equals(isVoted[i+1], isVoted[i]),
        Equals(logs_event[i+1], ProposalRejected),
        Equals(logs_arg1[i+1], noAddr),
        Equals(logs_arg2[i+1], BVZExt1(proposalID[i],UintWidth - 16)),
        Equals(logs_arg3[i+1], Zero),
        Equals(ether_balance[i+1], ether_balance[i]),
        Equals(now[i+1], time_delta[i])
    )

    no_consensus = And(
        Equals(daoTokensEmitted[i+1], daoTokensEmitted[i]),
        Equals(proposalID[i+1], proposalID[i]),
        Equals(votedYes[i+1], votedYes[i]),
        Equals(votedNo[i+1], votedNo[i]),
        Equals(proposal_recepient[i+1], proposal_recepient[i]),
        Equals(proposal_amount[i+1], proposal_amount[i]),
        Equals(proposal_deadline[i+1], proposal_deadline[i]),
        Equals(daoBalance[i+1], daoBalance[i]),
        Equals(isVoted[i+1], isVoted[i]),
        Equals(logs_event[i+1], ProposalNoConsensusYet),
        Equals(logs_arg1[i+1], msg_sender[i]),
        Equals(logs_arg2[i+1], BVZExt1(proposalID[i],UintWidth - 16)),
        Equals(logs_arg3[i+1], Zero),
        Equals(ether_balance[i+1], ether_balance[i]),
        Equals(now[i+1], time_delta[i])
    )
    return Ite(isConsensusReached,
               Ite(And(isPositiveDecision, isSufficientFunds),
                   executed, rejected), no_consensus)    

def erc20_transfer_precond(i):
    tokens = erc20_transfer_tokens_arg[i]
    SenderBalance = Select(daoBalance[i], msg_sender[i])
    ToBalance = Select(daoBalance[i], erc20_transfer_to_arg[i])
    Sum = BVAdd(ToBalance, tokens)
    HasVoted = Select(isVoted[i], Concat(proposalID[i], msg_sender[i]))
    return And(Not(Equals(msg_sender[i], erc20_transfer_to_arg[i])),
               BVUGE(SenderBalance, tokens),
               BVUGT(Sum, ToBalance),
               HasVoted == BoolFalse)

def erc20_transfer(i):
    tokens = erc20_transfer_tokens_arg[i]
    to_addr = erc20_transfer_to_arg[i]
    from_addr = msg_sender[i]
    SenderBalance = Select(daoBalance[i], from_addr)
    ToBalance = Select(daoBalance[i], to_addr)
    Sum = BVAdd(ToBalance, tokens)    
    daoBalance1 = Store(daoBalance[i], to_addr, Sum)
    daoBalance2 = Store(daoBalance1, msg_sender[i],
                        BVSub(SenderBalance, tokens))
    return And(
        Equals(daoTokensEmitted[i+1], daoTokensEmitted[i]),
        Equals(proposalID[i+1], proposalID[i]),
        Equals(votedYes[i+1], votedYes[i]),
        Equals(votedNo[i+1], votedNo[i]),
        Equals(proposal_recepient[i+1], proposal_recepient[i]),
        Equals(proposal_amount[i+1], proposal_amount[i]),
        Equals(proposal_deadline[i+1], proposal_deadline[i]),
        Equals(daoBalance[i+1], daoBalance2),
        Equals(isVoted[i+1], isVoted[i]),
        Equals(logs_event[i+1], ERC20_Transfer),
        Equals(logs_arg1[i+1], to_addr),
        Equals(logs_arg2[i+1], tokens),
        Equals(logs_arg3[i+1], Zero),
        Equals(ether_balance[i+1], ether_balance[i]),
        Equals(now[i+1], time_delta[i])
    )               

def execute_proposal_transition(i):
    return And(execute_proposal_precond(i),
               execute_proposal(i))

def refund_transition(i):
    return And(refund_precond(i),
               refund(i))

def vote_transition(i):
    return And(vote_precond(i),
               vote(i))

def deposit_transition(i):
    return And(deposit_precond(i),
               deposit(i))

def propose_transition(i):
    return And(propose_precond(i),
               propose(i))

def erc20_transfer_transition(i):
    return And(erc20_transfer_precond(i),
               erc20_transfer(i))

def transition(i):
    return Or(
        erc20_transfer_transition(i),
        propose_transition(i),
        deposit_transition(i),
        vote_transition(i),
        refund_transition(i),
        execute_proposal_transition(i))

def time_monotonicity(i):
    return BVULT(now[i], now[1 + i])

def initial_state():
    return And(
        Equals(daoTokensEmitted[0], daoTokensEmitted_initial),
        Equals(proposalID[0], proposalID_initial),
        Equals(votedYes[0], votedYes_initial),
        Equals(votedNo[0], votedNo_initial),
        Equals(daoBalance[0], daoBalance_initial),
        Equals(isVoted[0], isVoted_initial),
        Equals(proposal_recepient[0],
               proposal_recepient_initial),
        Equals(proposal_amount[0], proposal_amount_initial),
        Equals(proposal_deadline[0], proposal_deadline_initial),
        Equals(logs_event[0], logs_event_initial),
        Equals(logs_arg1[0], logs_arg1_initial),
        Equals(logs_arg2[0], logs_arg2_initial),
        Equals(logs_arg3[0], logs_arg3_initial),
        Equals(ether_balance[0],
               Store(ether_balance_initial,
                     miniDAO_contract_address, Zero)),
        Equals(now[0], now_initial))

def event_emitted(i, event_name, arg1, arg2, arg3):
    arg1_cond = TRUE()
    arg2_cond = TRUE()
    arg3_cond = TRUE()
    if arg1 is not None:
        arg1_cond = Equals(logs_arg1[i], arg1)
    if arg2 is not None:
        arg2_cond = Equals(logs_arg2[i], arg2)
    if arg3 is not None:
        arg3_cond = Equals(logs_arg3[i], arg3)
        
    return And(Equals(logs_event[i], event_name),
               arg1_cond, arg2_cond, arg3_cond)

#=========================================
# Generate any valid trace of given length
#=========================================
def any_trace_prop(solver):
    solver.push()    
    solver.check()
    solver.pop()
    m = solver.model()
    for i in range(NumOfSteps):
        print Log(m, i)
    
#=========================================
# No vote implies refund ability
#=========================================
def DepositedNotVotedRefund(solver, num_of_steps):
    investor   = Symbol('investor', AddressType)
    contractor = Symbol('contractor', AddressType)
    amountN    = Symbol('amountN', Uint) # for ProposalAdded
    amount     = Symbol('amount', Uint)   # for Deposited
    # counterexample when refund is made after two deposits!
    addedJ     = Symbol('addedJ', Uint5)
    depositedI = Symbol('depositedI', Uint5)
    refundZ    = Symbol('refundZ', Uint5)
    proposalN  = Symbol('proposalN', Uint)
    # exists i, 1 <= i < k . sigma[i].logs = Deposited(investor, amount)
    # translated into propositional form
    depositedProp = \
        Or([And([Equals(depositedI, BitVecVal(i, 5)),
                 event_emitted(i, Deposited, investor, amount, Zero)])
            for i in range(1, num_of_steps)])
    # exists i, 1 <= i < k . sigma[i].logs = ProposalAdded(...)
    proposalAddedProp = \
        Or([And([Equals(addedJ, BitVecVal(i, 5)),
                 event_emitted(i, ProposalAdded, msg_sender[i], amountN, proposalN)])
            for i in range(1, num_of_steps)])    
    # forall i, 1 <= i < k . sigma[i].logs ~= Voted(investor, ...)
    # The investor did not vote till the end of the trace 
    notVotedProp = \
        And([And(Not(event_emitted(i, Voted, investor, None, One)), 
                 Not(event_emitted(i, Voted, investor, None, Zero)))
             for i in range(1, num_of_steps)])

    # refund obviously will not be avaiable after refund is made.
    # so we need to rule out such scenarios
    # sigma[i].logs = Deposited(investor,...)
    # forall i, 1 <= i < k . sigma[j].logs ~= Refunded(investor,...)
    notRefundedConj = \
        And([Not(event_emitted(i, Refunded, investor, None, Zero))
             for i in range(1, num_of_steps)])
    noERC20TransfersConj = \
        And([Not(event_emitted(i, ERC20_Transfer, None, None, None))
             for i in range(1, num_of_steps)])
    # exists i : i > deposited_j /\ ~refund_precond(sigma[i],v,investor,...)
    refundNotEnabledProp = \
        Or([And(
            And(Equals(refundZ, BitVecVal(i, 5)), BVUGT(refundZ, depositedI)),
            And(Not(refund_precond(i)), Equals(investor, msg_sender[i])))
            for i in range(1, num_of_steps)])
    solver.add(initial_state())
    for i in range(num_of_steps):        
        solver.add(transition(i))
        solver.add(time_monotonicity(i))            
    solver.add(depositedProp, proposalAddedProp, notVotedProp,
               notRefundedConj, noERC20TransfersConj,
               refundNotEnabledProp)
    if solver.check() == unsat:
        return True
    model = solver.model()
    refundStep = model[refundZ]
    investorAddr = model[investor]
    # we start from 1 because there cant be any event in state 0
    for i in range(1, num_of_steps+1):
        daoBal = model[daoBalance[i]]
        ethBal = model[ether_balance[i]]
        print "%s. %s" % (i, Log(model, i))
        #print "daoBalance = %s" % daoBal
        #print "etherBalance = %s" % ethBal # str_ether_balance(ethBal)
    print "step = %s, investor = addr%s, proposalID = %s, depositedI = %s " % \
    (refundStep, investorAddr, model[proposalN], model[depositedI])
    return False

#===================================================
# Always SUM(daoBalance[i]) = daoTokensEmitted
#===================================================
def voted_sum_le_balance_inv(i):
    BalanceSum = sum([Select(daoBalance[i], BitVecVal(k, AddrWidth))
                      for k in range(1, NumOfAddresses+1)])
    return Equals(BalanceSum, daoTokensEmitted[i])

def CheckInvDaoBalanceEquTokens(solver):
    for i in range(2):
        solver.add(
            Not(Equals(noAddr, propose_recepient_arg[i])))
        solver.add(
            Not(Equals(miniDAO_contract_address, msg_sender[i])))
        solver.add(Not(Equals(msg_sender[i], noAddr)))
        solver.add(
            BVUGT(msg_sender[i], noAddr))
        solver.add(
            BVULT(msg_sender[i], miniDAO_contract_address))
        solver.add(
            Not(Equals(noAddr, erc20_transfer_to_arg[i])))
        solver.add(
            BVULT(erc20_transfer_to_arg[i], miniDAO_contract_address))
        solver.add(
            BVUGT(vote_proposalID_arg[i], 0))
    solver.push()
    solver.add(initial_state())  # sigma_0 in I    
    solver.add(Not(voted_sum_le_balance_inv(0)))
    print "Checking base case."   
    if solver.check() == sat:
        print "Initial state does not respect the invariant property"
    else:
       solver.pop()
       print "Checking inductive step."
       solver.add(transition(0))
       solver.add(time_monotonicity(0))
       solver.add(voted_sum_le_balance_inv(0))
       solver.add(Not(voted_sum_le_balance_inv(1)))
       if solver.check() == sat:
            model = solver.model()        
            daoTokEmitted1 = model[daoTokensEmitted[0]]
            daoTokEmitted2 = model[daoTokensEmitted[1]]            
            daoBal1 = model[daoBalance[0]]
            daoBal2 = model[daoBalance[1]]
            print "%s. %s" % (1, Log(model, 0))
            print "daoTokEmitted1=", daoTokEmitted1
            print "daoBalance1=", daoBal1
            print "%s. %s" % (2, Log(model, 1))
            print "daoTokEmitted2=", daoTokEmitted2
            print "daoBal2=", daoBal2
       else:
            print "Invariant holds. No counter-example found."

def CheckRejectedNotExecuted(solver, start):
    for k in range(start, NumOfSteps + 1):
        solver.push()        
        for i in range(k):
            solver.add(
                Not(Equals(noAddr, propose_recepient_arg[i])))
            solver.add(
                Not(Equals(miniDAO_contract_address, msg_sender[i])))
            solver.add(Not(Equals(msg_sender[i], noAddr)))
            solver.add(
                BVUGT(msg_sender[i], noAddr))
            solver.add(
                BVULT(msg_sender[i], miniDAO_contract_address))
            solver.add(
                Not(Equals(noAddr, erc20_transfer_to_arg[i])))
            solver.add(
                BVULT(erc20_transfer_to_arg[i], miniDAO_contract_address))
            solver.add(
                BVUGT(vote_proposalID_arg[i], 0))                       
        print "Checking trace of length %s..." % k
        if RejectedNotExecuted(solver, k) == False:        
            break
        solver.pop()
        
def RejectedNotExecuted(solver, start):
    solver.add(initial_state())
    for i in range(NumOfSteps - 1):        
        solver.add(transition(i))
        solver.add(time_monotonicity(i))
    proposalAddedI = Symbol('proposalAddedI', Uint)
    proposalRejectedJ = Symbol('proposalRejectedJ', Uint)
    proposalExecutedK = Symbol('proposalExecutedK', Uint)
    proposalIDSym = Symbol('proposalIDSym', Uint)
    proposalAdded = \
        Or([And(Equals(proposalAddedI, BitVecVal(i, BitWidth)),
                event_emitted(i, ProposalAdded, None, None, proposalIDSym))
            for i in range(1, NumOfSteps)])
    solver.add(proposalAdded)
    proposalRejected = \
        Or([And(Equals(proposalRejectedJ, BitVecVal(i, BitWidth)),
                event_emitted(i, ProposalRejected, None, proposalIDSym, Zero))
            for i in range(1, NumOfSteps)])
    solver.add(proposalRejected)
    solver.add(And(proposalExecutedK > proposalAddedI,
                   proposalExecutedK < proposalRejectedJ))
    proposalExecuted = \
        Or([And(Equals(proposalExecutedK, BitVecVal(i, BitWidth)),
                event_emitted(i, ProposalExecuted, None, proposalIDSym, Zero))
            for i in range(1, NumOfSteps)])
    solver.add(proposalExecuted)
    if solver.check() == unsat:
        print "Property holds: no counter-example found."
    else:
        print "Property does not hold"
        model = solver.model()
        for i in range(NumOfSteps):
            print "%s. %s" % (i, Log(model, i))        
    
def main():
   logic="ALL"
   solver = SolverFor(logic)   
   #CheckDepositedNotVotedRefund(solver, 12)
   CheckInvDaoBalanceEquTokens(solver)       
   #CheckRejectedNotExecuted(solver, 8)

def CheckDepositedNotVotedRefund(solver, start):
    for k in range(start, NumOfSteps + 1):
        solver.push()        
        for i in range(k):
            solver.add(
                Not(Equals(noAddr, propose_recepient_arg[i])))
            solver.add(
                Not(Equals(miniDAO_contract_address, msg_sender[i])))
            solver.add(Not(Equals(msg_sender[i], noAddr)))
            solver.add(
                BVUGT(msg_sender[i], noAddr))
            solver.add(
                BVULT(msg_sender[i], miniDAO_contract_address))
            solver.add(
                Not(Equals(noAddr, erc20_transfer_to_arg[i])))
            solver.add(
                BVULT(erc20_transfer_to_arg[i], miniDAO_contract_address))
            solver.add(
                BVUGT(vote_proposalID_arg[i], 0))                       
        print "Checking trace of length %s..." % k
        if DepositedNotVotedRefund(solver, k) == False:        
            break
        solver.pop()    

def Log(model, i):
    name = model[logs_event[i]]
    if i > 0:
        _from =model[msg_sender[i - 1]]
    arg1 = model[logs_arg1[i]]
    arg2 = model[logs_arg2[i]]
    arg3 = model[logs_arg3[i]]

    if name == NoEvent:
        return "NoEvent"
    elif name == ProposalAdded:
        return "ProposalAdded, sender = addr%s, amount = %s, proposalID = %s" \
        % (arg1, arg2, arg3)
    elif name == ProposalExecuted:
        return "ProposalExecuted, proposalID = %s" % arg2
    elif name == ProposalRejected:
        return "ProposalRejected, proposalID = %s" % arg2
    elif name == ProposalNoConsensusYet:
        return "ProposalNoConsensusYet"
    elif name == Voted:
        return "Voted, sender = addr%s, proposalID = %s, supports=%s" % (arg1, arg2, arg3)
    elif name == Refunded:
        return "Refunded, sender = addr%s, tokens = %s" \
                % (arg1, arg2)
    elif name == Deposited:
        return "Deposited, sender = addr%s, tokens = %s" \
                % (arg1, arg2)
    elif name == ERC20_Transfer:
        return "ERC20_Transfer, from = addr%s, to = addr%s, tokens = %s" \
    % (_from, arg1, arg2)
    else:
        return "Unknown"

if __name__== "__main__":
  main()
