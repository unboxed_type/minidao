const MiniDAO = artifacts.require("MiniDAO");
const zp = require('./expectThrow');

// a helper function to change current time
// it is used to test deadline conformance
const increaseTime = addSeconds => {
    web3.currentProvider.send({
        jsonrpc: "2.0", 
        method: "evm_increaseTime", 
        params: [addSeconds], id: 0
    })
}
contract('MiniDAO', async (accounts) => {
    let dao;
    let DAO_token_price = 1000000;
    //create new smart contract instance before each test method
    beforeEach(async function() { dao = await MiniDAO.new(); });

    // contractor is able to add a proposal at the initial state
    it("add proposal at the initial state", async() => {
	let expected_sum = 8 * DAO_token_price;
	let acc0 = accounts[0];
	let result = await dao.propose(acc0, expected_sum,
				       "my project", {from: acc0});
	let propId = await dao.proposalID();
	assert.equal(result.logs[0].event, 'ProposalAdded');
	assert.equal(result.logs[0].args.description, "my project");
	assert.equal(result.logs[0].args.amount, expected_sum);
	assert.equal(result.logs[0].args.proposalID, propId.toNumber());	
    });
    
    // 1 inverstor, 1 contractor, proposal is executed
    // we check that recepient receives needed amout of wei
    // and event is added into log
    it("add proposal, vote, execute proposal", async() => {
	const expected_sum = 8 * DAO_token_price;
	let acc0 = accounts[0];
	let acc1 = accounts[1];
	await dao.deposit({from: acc0, value: 10 * DAO_token_price});
	await dao.propose(acc1, expected_sum, "my project", {from: acc1});
	let propId = await dao.proposalID();
	await dao.vote(propId, true, {from: acc0});
	const old_balance = web3.eth.getBalance(acc1);

	// console.log(`old balance: ${old_balance.toString()}`);

	const result = await dao.execute_proposal({from: acc1});	
	const gasUsed = result.receipt.cumulativeGasUsed;
	const gasPrice = web3.eth.getTransaction(result.tx).gasPrice;
	// console.log(`gas used = ${gasUsed.toString()},
	// gas price = ${gasPrice.toString()}`);

	const new_balance = web3.eth.getBalance(acc1).toNumber();

	// console.log(`new balance = ${new_balance.toString()}`);
	
	assert.equal(result.logs[0].event, 'ProposalExecuted');
	assert.equal(result.logs[0].args.proposalID, propId.toNumber());	
	assert.equal(new_balance,
		     old_balance.add(expected_sum)
		     .sub(gasPrice.mul(gasUsed)));
    });
    
    // 2 inverstors, 1 contractor, no consensus, thus proposal is rejected
    // we check that corresponding event is added into log
    it("add proposal, 2 votes, no consensus, proposal is rejected", async() => {
	let cont_acc = accounts[2];
	let acc0 = accounts[0];
	let acc1 = accounts[1];
	await dao.deposit({from: acc0, value: 10 * DAO_token_price});
	await dao.deposit({from: acc1, value: 12 * DAO_token_price});
	await dao.propose(cont_acc, 8 * DAO_token_price,
			  "my project", {from: cont_acc});
	let propId = await dao.proposalID();
	await dao.vote(propId, true, {from: acc0});
	await dao.vote(propId, false, {from: acc1});
	let result = await dao.execute_proposal({from: acc0});
	assert.equal(result.logs[0].event, 'ProposalRejected');
	assert.equal(result.logs[0].args.proposalID, propId.toNumber());
    });

    // 3 inverstors, 1 contractor, consensus is reached
    // we check that corresponding event is added into log
    it("add proposal, 3 votes, consensus reached, proposal is executed", async() => {
	let cont_acc = accounts[3];
	let acc0 = accounts[0];
	let acc1 = accounts[1];
	let acc2 = accounts[2];
	await dao.deposit({from: acc0, value: 10 * DAO_token_price});
	await dao.deposit({from: acc1, value: 12 * DAO_token_price});
	await dao.deposit({from: acc2, value: 10 * DAO_token_price});
	await dao.propose(cont_acc, 22 * DAO_token_price,
			  "my project", {from: cont_acc});
	let propId = await dao.proposalID();
	await dao.vote(propId, true, {from: acc0});
	await dao.vote(propId, false, {from: acc1});
	await dao.vote(propId, true, {from: acc2});
	let result = await dao.execute_proposal({from: cont_acc});
	assert.equal(result.logs[0].event, 'ProposalExecuted');
	assert.equal(result.logs[0].args.proposalID, propId.toNumber());
    });

    // 3 inverstors, 1 contractor, 1 inverstors wants its money back thru refund
    // we check that corresponding event is added into log
    it("add proposal, 2 votes, refund is asked by one of investors", async() => {
	let cont_acc = accounts[3];
	let acc0 = accounts[0];
	let acc1 = accounts[1];
	let acc2 = accounts[2];
	await dao.deposit({from: acc0, value: 10 * DAO_token_price});
	await dao.deposit({from: acc1, value: 12 * DAO_token_price});
	await dao.deposit({from: acc2, value: 10 * DAO_token_price});
	await dao.propose(cont_acc, 22 * DAO_token_price,
			  "my project", {from: cont_acc});
	let old_balance = web3.eth.getBalance(acc2);
	let result = await dao.refund({from: acc2});	 
	let propId = await dao.proposalID();
	let new_balance = web3.eth.getBalance(acc2);
	const gasUsed = result.receipt.cumulativeGasUsed;
	const gasPrice = web3.eth.getTransaction(result.tx).gasPrice;
	assert.equal(result.logs[0].event, 'Refunded');
	assert.equal(result.logs[0].args.investor, acc2);
	assert.equal(result.logs[0].args.tokens, 10);
	assert.equal(new_balance.toNumber(),
		     old_balance.sub(gasPrice.mul(gasUsed))
		     .add(10*DAO_token_price).toNumber());
    });

    // 1 investor, 1 contractor, voting after deadline causes an exception
    it("add proposal, increase time by 2 weeks, vote, deadline exception", async() => {
	let cont_acc = accounts[3];
	let acc0 = accounts[0];	
	await dao.deposit({from: acc0, value: 10 * DAO_token_price});
	await dao.propose(cont_acc, 8 * DAO_token_price,
			  "my project", {from: cont_acc});
	let propId = await dao.proposalID();
	increaseTime(2 * 7 * 24 * 3600); // 2 weeks
	await zp.expectThrow(dao.vote(propId, true, {from: acc0}));
    });

    // 1 investors, 1 contractor, refunding after voting causes an exception
    it("add proposal, vote, refund and observe exception", async() => {
	let cont_acc = accounts[3];
	let acc0 = accounts[0];	
	await dao.deposit({from: acc0, value: 10 * DAO_token_price});
	await dao.propose(cont_acc, 8 * DAO_token_price,
			  "my project", {from: cont_acc});
	let propId = await dao.proposalID();
	dao.vote(propId, true, {from: acc0})	
	await zp.expectThrow(dao.refund({from:acc0}));
    });

    it("deposit less than 1 miniDAO token worth, observe exception", async() => {
	let acc0 = accounts[0];	
	await zp.expectThrow(dao.deposit({from:acc0, value: DAO_token_price - 1}));
    });

    it("aliquant part of the deposit amount is returned", async() => {
	let acc0 = accounts[0];
	let old_balance = web3.eth.getBalance(acc0);	
	let result = await dao.deposit({from: acc0, value: 2*DAO_token_price - 1});
	let new_balance = web3.eth.getBalance(acc0);
	const gasUsed = result.receipt.cumulativeGasUsed;
	const gasPrice = web3.eth.getTransaction(result.tx).gasPrice;
	assert.equal(new_balance.toNumber(),
		     old_balance.sub(gasPrice.mul(gasUsed))
		     .sub(DAO_token_price).toNumber());	
    });

    it("only one proposal at a time can be considered in the voting process", async() => {
	let cont_acc = accounts[2];
	let acc0 = accounts[0];
	let acc1 = accounts[1];
	let acc2 = accounts[2];
	await dao.deposit({from: acc0, value: 10 * DAO_token_price});
	await dao.deposit({from: acc1, value: 12 * DAO_token_price});
	await dao.propose(cont_acc, 8 * DAO_token_price,
			  "first project", {from: cont_acc});
	await zp.expectThrow(dao.propose(acc2, 10*DAO_token_price,
					 "second project", {from: acc2}));
    });

    it("erc20 transfer after vote is not avaialbe", async() => {
	let cont_acc = accounts[2];
	let acc0 = accounts[0];
	let acc1 = accounts[1];
	let acc2 = accounts[2];
	await dao.deposit({from: acc0, value: 10 * DAO_token_price});
	await dao.propose(cont_acc, 8 * DAO_token_price,
			  "first project", {from: cont_acc});
	let propId = await dao.proposalID();
	await dao.vote(propId, true, {from: acc0});	
	await zp.expectThrow(dao.transfer(acc2, 10, {from: acc0}));
    });    
})
