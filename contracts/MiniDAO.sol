// =============================================================================
//  High level description of the miniDAO business logic 
// =============================================================================

// * Investors buy miniDAO tokens to be able to vote for or against contractor's 
//   proposals. 

// * To buy DAO tokens, an investor sends ether into miniDAO contract. Upon 
//   receiving ether, the sender's internal balance is adjusted with a proper 
//   amount of miniDAO tokens. The price of miniDAO token is fixed.
//   The remainder of ether that has not been spent is returned to the sender.

// * Investor's voting power is proportional to the amount of miniDAO tokens in 
//   possession of the investor. 

// * A contractor sends proposal into MiniDAO with the following info:
//    - the address to send funds to in case the proposal is executed
//    - the required amount of Ether for the proposed project
//   If proposal is successfuly registered then a unique proposal ID is 
//   returned. 

// * After a proposal has been sent, inverstors vote for or against the given 
//   proposal. Investor can choose not to participate in the voting process
//   at all.

// * Upon receiving enough votes (this fraction is called a consensus), the 
//   final decision about investing is made depending on whether the 
//   majority of votes are given for or against the proposal and the proposal
//   gets executed.

// * Voting process is limited with a fixed deadline of 2 weeks. If not executed,
//   after that time the proposal will be rejected even if enough votes were given
//   and there are sufficient funds to support the proposal.

// * If consensus is reached, then the required amount of ether is transfered 
//   from the miniDAO contract to the recepient address.

// * If consensus is not reached before a deadline, then the proposal 
//   is rejected.

// * To make investing process look less risky, we claim that an investor
//   can get a refund if he didn't vote for any proposal.

// * Only one proposal can be processed at a time. This is artificial 
//   restriction to make things simpler.

// * If no proposal is set, investors are still able to buy miniDAO tokens.

// * A total number of DAO tokens is not limited.

// * A consensus fraction is a constant number.

//  LATER:
// // * A consensus fraction is calculated based on amount of ether the contractor
// //  would like to get from the miniDAO. The more ether the contractor expects
// //  for his project, the higher that value will be.

// * After the proposal gets executed or rejected, the MiniDAO contract is 
//   ready to receive next proposal.

// * Being ERC20 compatible, miniDAO tokens can be transfered from/to other
//   accounts.

// * Keep in mind that even if you do not want to invest in some particular 
//   project, if the majority decides to invest, then your money will be 
//   sent to the proposal recepient address anyway. But lets pretend we
//   have overlooked that property. 

contract ERC20Interface {
    function totalSupply() public view  returns (uint);
    function balanceOf(address tokenOwner) public view  returns (uint balance);
    function allowance(address tokenOwner, address spender) public view 
        returns (uint remaining);
    function transfer(address to, uint tokens) public  returns (bool success);
    function approve(address spender, uint tokens) public  returns (bool success);
    function transferFrom(address from, address to, uint tokens) public 
        returns (bool success);
    event Transfer(address from, address to, uint tokens);
    event Approval(address tokenOwner, address spender, uint tokens);
}

contract MiniDAOInterface {
    function deposit() public payable;
    function vote(uint proposalId, bool supportsProposal) public;
    function refund() public;
    function propose(address recepient, uint amount, string text) public;
    function execute_proposal() public;
    
    event Voted(address voter, uint proposalID, bool supportsProposal);
    event Refunded(address investor, uint tokens);
    event Deposited(address investor, uint tokens);
    event ProposalAdded(uint amount, uint proposalID);
    event ProposalExecuted(uint proposalID);
    event ProposalRejected(uint proposalID);
}

contract MiniDAO is MiniDAOInterface, ERC20Interface {
    uint constant consensus = 51;
    uint constant DAO_token_price = 10**6 wei;
    uint DAO_tokens_emitted= 0;
    uint public proposalID = 1; // unique id
    uint voted_yes = 0;
    uint voted_no = 0;
    mapping (address => uint) balance; // miniDAO tokens balance

    // address is not allowed to transfer tokens
    // if found in this list
    // value is a proposal ID
    mapping (address => uint) blocked; 
    struct Proposal {
        address recepient;
        uint amount;
        uint deadline;
    }
    
    Proposal proposal;

    // proposalID = 0 is reserved to signal the fact of vote for any proposal for given
    // address. Used to block refunds after vote.
    mapping (uint => mapping(address => bool)) isVoted; // proposalID => mapping (address=>bool)

    function totalSupply() public view  returns (uint) {
        return DAO_tokens_emitted;
    }
    function balanceOf(address tokenOwner) public view  returns (uint) {
        return balance[tokenOwner];
    }
    function allowance(address /*tokenOwner*/, address /*spender*/) public view 
        returns (uint) {
        revert();
    }
    function transfer(address to, uint tokens) public  returns (bool) {
        require (balance[msg.sender] >= tokens);
	require (balance[to] + tokens > balance[to]);
	require (blocked[msg.sender] != proposalID);
	require (to != msg.sender);
        balance[to] += tokens;
        balance[msg.sender] -= tokens;
        emit Transfer(msg.sender, to, tokens);
        return true;
    }
    function approve(address /*spender*/, uint /*tokens*/) public  returns (bool) {
        revert();
    }
    function transferFrom(address /*from*/, address /*to*/, uint /*tokens*/) public 
        returns (bool)  {
        revert();
    }

    function propose(address recepient, uint amount, string text) public {
        require(proposal.recepient == address(0));
        require(recepient != address(0));
        require(amount != 0);
        //require(amount <= address(this).balance);
	proposal.recepient = recepient;
        proposal.amount = amount;
        proposal.deadline = now + 2 weeks;	
        emit ProposalAdded(amount, proposalID);
    }

    function deposit() public payable {
        uint tokens = msg.value / DAO_token_price;
        address sender = msg.sender;	      
        require (msg.value >= DAO_token_price); 
	require (balance[sender] + tokens > balance[sender]);
	require (DAO_tokens_emitted + tokens >
		 DAO_tokens_emitted);
        uint remainder = msg.value - tokens * DAO_token_price;
	
        balance[sender] += tokens;
	DAO_tokens_emitted += tokens;
	
        if (remainder > 0)
            sender.transfer(remainder);
	emit Deposited(sender, tokens);
    }

    function vote(uint voteProposalID, bool supportsProposal) public {
        // TOD bug can be introduced if we omit voteProposalID
        address sender = msg.sender;
        uint tokens = balance[sender];
	
        require(voteProposalID == proposalID);
        require(proposal.recepient != address(0));
        require(now < proposal.deadline);
        require(tokens > 0);
        require(isVoted[proposalID][sender] == false);
	require(voted_yes + tokens > voted_yes);
	require(voted_no + tokens > voted_no);
	
        if (supportsProposal)
            voted_yes += tokens;
        else
            voted_no += tokens;
	blocked[msg.sender] = proposalID;
	isVoted[0][sender] = true;  // to be able to block refunds after any vote
        isVoted[proposalID][sender] = true;
	emit Voted(sender, proposalID, supportsProposal);
    }

    function refund() public {
        address sender = msg.sender;
        uint tokens = balance[sender];
	
        require (isVoted[0][sender] == false);
        require (tokens > 0);
	require (DAO_tokens_emitted >= tokens);

        DAO_tokens_emitted -= tokens;
        balance[sender] = 0;
        sender.transfer(tokens * DAO_token_price);
	emit Refunded(sender, tokens);
    }

    function execute_proposal() public {
        address recepient = proposal.recepient;
        uint amount = proposal.amount;
	
        require(recepient != address(0));

	if (now >= proposal.deadline) {
	  emit ProposalRejected(proposalID);
	  reset_proposal();
	  return;
	}

	bool isConsensusReached = 
	  100 * (voted_yes + voted_no) / DAO_tokens_emitted >=
	  consensus;
	bool isSufficientFunds =
	  amount <= address(this).balance;
	bool isPositiveDecision =
	  voted_yes > voted_no;
	
	if (isConsensusReached) {
	  if (isSufficientFunds && isPositiveDecision) {
	    recepient.transfer(amount);
	    emit ProposalExecuted(proposalID);
	  }
	  else
	    emit ProposalRejected(proposalID);
	  reset_proposal();
	}
    }

    function reset_proposal() internal {
	assert (proposalID + 1 > proposalID);
        voted_yes = 0;
        voted_no  = 0;	
        proposalID++;
        proposal = Proposal(address(0), 0, 0);
    }
}

// Some functional requirements we would like to check:

// #AddedRejectedNoExecute
// If proposal i was Added and the proposal was later Rejected
// then it was never the case that the proposal was Executed
// somewhere in between Added and Rejected events.

// #NotVotedRefund
// If an investor deposited some tokens, he will be able to
// get a full refund if he did not vote for the current proposal
